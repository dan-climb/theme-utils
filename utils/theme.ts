import { ThemedStyledProps } from "styled-components";
import { Colors, Components, Theme } from "../types/theme";

type PathsToStringProps<T> = T extends string
  ? []
  : {
      [K in Extract<keyof T, string>]: [K, ...PathsToStringProps<T[K]>];
    }[Extract<keyof T, string>];

type Join<T extends string[], D extends string> = T extends []
  ? never
  : T extends [infer F]
  ? F
  : T extends [infer F, ...infer R]
  ? F extends string
    ? `${F}${D}${Join<Extract<R, string[]>, D>}`
    : never
  : string;

type ComponentPaths = Join<PathsToStringProps<Components>, ".">;

interface ThemeColorOptions {
  color: ((args: any) => keyof Colors) | keyof Colors;
  component?: ComponentPaths;
  transform?: (color: string) => string;
}

const resolve = (obj: any, selector: string | undefined) =>
  !selector
    ? null
    : selector.split(".").reduce((acc, cur) => (acc ? acc[cur] : null), obj);

export const getThemeColor =
  ({ color, component, transform }: ThemeColorOptions) =>
  ({ theme, ...rest }: ThemedStyledProps<any, Theme>) => {
    const derivedColor =
      theme?.colors[typeof color === "function" ? color(rest) : color];
    const componentColor = resolve(theme?.components, component);

    if (!transform) return componentColor || derivedColor;

    return transform(componentColor || derivedColor);
  };
