# TypeScript & Styled Components Theme Utils

A cool little idea for providing overrides, fallbacks and defaults to styled components in a Theme Provider

## Usage

`getThemeColor` - (options: Options) => ({ theme, ...rest }: StyledComponentArgs) => string

`Options`
- `color` - (args: StyledComponentArgs) => keyof Colors | keyof Colors
- `component` - Dot notation string of component path

---

Basic example

```javascript
import { getThemeColor } from 'utils/theme'

const Dropdown = styled.div`
    background-color: ${getThemeColor({ color: 'primary' })};
`
```

---

With props

```javascript
import { getThemeColor } from 'utils/theme'

const Dropdown = styled.div<{ open: boolean }>`
    background-color: ${getThemeColor({ color: ({ open }) => 'primary' : 'secondary' })};


```

---

With component override (Most components should have this but it is not required in the theme)

```javascript
import { getThemeColor } from 'utils/theme'

const Dropdown = styled.div`
    background-color: ${getThemeColor({ 
        color: 'primary',
        component: 'select.dropdown.color' 
    })};
`
```

