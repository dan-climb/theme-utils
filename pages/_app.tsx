import "@fontsource/karla/400.css";

import React from "react";
import { AppProps } from "next/app";
import Head from "next/head";
import styled, { createGlobalStyle, ThemeProvider } from "styled-components";

import { Theme } from "../types/theme";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Theme Utils</title>
      </Head>
      <div>
        <Global />
        <ThemeProvider theme={theme}>
          <Main>
            <Component {...pageProps} />
          </Main>
        </ThemeProvider>
      </div>
    </>
  );
}

const theme: Theme = {
  colors: {
    primary: "#ff0000",
    secondary: "#0000ff",
  },
  components: {
    select: {
      dropdown: {
        color: "#ffff00",
        background: "#ff00ff",
      },
    },
  },
};

const Global = createGlobalStyle`
    * {
      font-family: 'Karla', Arial, Helvetica, sans-serif;
    }

    body {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        color: #3B444A;
    }

    form > label {
      display: block;
      margin-top: 1rem;
    }
`;

const Main = styled.main`
  padding: 3rem;
  margin: 0 auto;
  max-width: 800px;
  width: 100%;
`;
