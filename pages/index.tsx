import styled from "styled-components";
import { lighten } from "polished";

import { getThemeColor } from "../utils/theme";

export default function Home() {
  return (
    <>
      <Title>Hello</Title>
      <Tagline>This is pretty</Tagline>
      <Tagline active>dang cool</Tagline>
      <Box>This box uses the primary color but is transformed with lighten</Box>
    </>
  );
}

// Provide the theme color and a component path override
const Title = styled.h1`
  color: ${getThemeColor({
    color: "primary",
    component: "select.dropdown.background",
  })};
  font-size: 50px;
`;

// color arg can be string (key of colors) or a function that has
// all args from the styled component to return a string
const Tagline = styled.h2<{ active?: boolean }>`
  color: ${getThemeColor({
    color: ({ active }) => (active ? "primary" : "secondary"),
  })};
  font-size: 36px;
`;

// getThemeColor can take a transform function that will get the color
// and return a new color e.g. transform: (color) => newColor
// this allows us to take advantage of polished with their curried functions
const Box = styled.div`
  padding: 1rem;
  width: 300px;
  height: 200px;
  color: white;
  background-color: ${getThemeColor({
    color: "primary",
    transform: lighten(0.2),
  })};
  box-sizing: border-box;
`;
