export interface Colors {
  primary: string;
  secondary: string;
}

export interface Components {
  select?: {
    dropdown?: {
      color?: string;
      background?: string;
    };
  };
}

export interface Theme {
  colors: Colors;
  components?: Components;
}
